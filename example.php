<?php

require_once('vendor/autoload.php');

use PMP\Plugins\DataTable;

$dataTable = new DataTable();

//Set DataTable columns - Required
$dataTable->setColumns([
    [
        'visible' => true, //column is vissible into html table
        'data' => 'as_id', // column alias name SELECT ALTA.id AS as_id
        'searchable' => false, // not used into search query
        'orderable' => true,//can order table 
        'width' => '1%', //html table td width , % or px
        'title' => 'id', //HTML column name
        'name' => 'ALTA.id', //real db column name with alias (eg. table A in select clause) - aliases are recommended especially when using Query builder and join tables
        'class' => 'text-left', //class to apply in html table td 
        'defaultOrder' => true, //default order when loading datatable
        'orderDirection' => 'DESC'//default order direction
    ],
    [
        'visible' => 0, //column is NOT vissible into html table
        'data' => 'as_other_table_id', // column alias name SELECT ALTB.id AS as_other_table_id
        'searchable' => false,
        'orderable' => false,
        'name' => 'ALTB.id'//real db column name with alias (eg. table B in select clause)
    ],
    [
        'visible' => 0, //column is not vissible into html
        'data' => 'column_three',
        'searchable' => true, // can be used in search query
        'orderable' => false,
        'name' => 'ALTB.description'
    ],
    [
        'visible' => '1',
        'data' => 'extra_column',
        'width' => '8%',
        'title' => 'Extra Column',
        'class' => '',
        'name' => false, //set name to false when column is not data base column but column is showed into html.The content can be added after query execution
        'searchable' => false,
        'orderable' => false
    ]
]);

//Set Data Table lenght menu - NOT REQUIRED , default is 
$dataTable->setLengthMenu([5, 10, 15, 20, 100]);

//Set Data Table rows per page - NOT REQUIRED ,default is 10
$dataTable->setPageLength(20);

//Set Data Table responsive - NOT REQUIRED, default is false
$dataTable->setResponsive(true);

//Set Data Table scrollable vertically with max height - NOT REQUIRED
$dataTable->setScrollable(500);


//Example if using Phacon Query Builder

/* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
$dataSource = new \Phalcon\Mvc\Model\Query\Builder();

$dataSource->columns($dataTable->getQueryColumns());

//$dataTable->getSearchedValue() - returns Searched value from Data Table filter with bind parameter
//$dataTable->getSearchQueryColumns() - returns string, all the defined columns where searchable is true (column1 LIKE :searched_column: OR column2 LIKE :searched_column:)

$dataSource->andWhere($dataTable->getSearchQueryColumns(), $dataTable->getSearchedValue());

//$dataTable->getOrderColumn() - returns string(must be checked if empty before to used in Query builder) - the column name by which to order. If is not defined into post from DataTable, the default column is used

//$dataTable->getOrderBy() - returns string - order direction. If is not defined into post from DataTable, the default direction is used
$dataSource->orderBy($dataTable->getOrderColumn() . ' ' . $dataTable->getOrderBy());


//$dataSource->getQuery()->execute();