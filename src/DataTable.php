<?php

namespace PMP\Plugins;

use Phalcon\Text;
use Phalcon\Config;

/**
 * 
 * Phalcon DataTable plugin.
 *   
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 * 
 */
class DataTable {

    /**
     * @var array Columns needed to render the Data table
     */
    protected $columns = [];

    /**
     * @var array Columns participate in db Query selection
     */
    protected $queryColumns = [];

    /**
     * @var array List with columns participate in search IN clause
     */
    protected $searchQueryColumns = [];

    /**
     * @var array Default Data Table length menu
     */
    protected $lengthMenu = [5, 10, 50, 70, 100];

    /**
     * @var number data table scroll vertically in pixels
     */
    protected $scrollY = false;

    /**
     * @var bool scroll Collapsible
     */
    protected $scrollCollapse = false;

    /**
     * @var bool Responsive data table
     */
    protected $responsive = false;

    /**
     * @var bool Data Table auto width
     */
    protected $autoWidth = false;

    /**
     * @var number default page length
     */
    protected $pageLength = 10;

    /**
     * @var array searched value
     */
    protected $searchedValue;

    /**
     * @var string order column names
     */
    protected $orderColumns;

    /**
     * @var string order direction
     */
    protected $defaultOrderDirection = 'ASC';

    /**
     * @var string order direction
     */
    protected $orderDirection = false;

    /**
     * @var \Phalcon\Config Post data from Data table
     */
    protected $postData;

    /**
     * @var [] Post data from Data table
     */
    protected $options = [];

    /**
     * @var [] Post data from Data table
     */
    protected $styles;

    /**
     * @param array $options ['columns' => array, length-menu => array]
     */
    public function __construct($options = []) {
        
        foreach ($options as $prop => $value) {

            $property = 'set' . Text::camelize($prop);
            
            if (method_exists(get_class(), $property)) {

                $this->{$property}($value);
            }
        }
    }

    /**
     * Data table options
     * 
     * @param array $data
     */
    public function setOptions($data = []) {

        $this->options = $data;
    }

    /**
     * Data table styles
     * 
     * @param array $data
     */
    public function setStyles($data = []) {

        $this->styles = $data;
    }

    /**
     * Set the post from Data table
     * 
     * @param array $post
     */
    public function setPostData($post = []) {

        $this->postData = new Config($post);
    }

    /**
     * Set Data Table scroll vertically.
     * 
     * @param number $pixels Maximum data table height
     */
    public function setScrollable($pixels = false) {

        if (empty($pixels) || !is_numeric($pixels)) {
            return false;
        }

        $this->scrollCollapse = true;

        $this->scrollY = $pixels . 'px';
    }

    /**
     * Set Responsive Data Table
     * @param bool $responsive 
     */
    public function setResponsive($responsive = false) {

        $this->responsive = $responsive ? true : false;
    }

    /**
     * Set Data Table to Auto Width
     * @param bool $autoWidth 
     */
    public function setAutoWidth($autoWidth = false) {

        $this->autoWidth = $autoWidth ? true : false;
    }

    /**
     * Set default page length
     * @param number pageLength 
     */
    public function setPageLength($pageLength = false) {

        if (empty($pageLength) || !is_numeric($pageLength)) {
            return false;
        }

        $this->pageLength = $pageLength;
    }

    /**
     * @param string $order_dir Set default order direction
     */
    public function setOrderDirection($order_dir = false) {

        $this->orderDirection = \strtoupper($order_dir);
    }

    /**
     * @param array $columns Columns needed to render the Data table
     */
    public function setColumns($columns = []) {

        if (empty($columns) || !is_array($columns)) {
            return false;
        }

        $this->columns = $columns;
    }

    /**
     * Set default Data table length menu
     * @param array|string $lengthMenu [5,10,15,20] or 5,10,15,20
     */
    public function setLengthMenu($lengthMenu = false) {

        if (empty($lengthMenu)) {
            return false;
        }

        $params = is_array($lengthMenu) ? $lengthMenu : explode(',', $lengthMenu);

        if (empty($params)) {
            return false;
        }

        $this->lengthMenu = $params;
    }

    /**
     * @return \Phalcon\Config
     */
    public function getPostData() {

        return $this->postData instanceof \Phalcon\Config ?
                $this->postData :
                new Config([]);
        
    }

    /**
     * @return string Order direction
     */
    public function getOrderDirection() {

        return !empty($this->orderDirection) ? $this->orderDirection : $this->defaultOrderDirection;
    }

    /**
     * @param array $options Data table options
     */
    public function getOptions() {

        $options = [
            'scrollY' => $this->scrollY,
            'scrollCollapse' => $this->scrollCollapse,
            'responsive' => $this->responsive,
            'autoWidth' => $this->autoWidth,
            'pageLength' => $this->pageLength,
            'lengthMenu' => $this->lengthMenu,
            'styles' => $this->styles,
            'order' => [
                [0, $this->getOrderDirection()]
            ]
        ];
        
        $options = array_merge($options, $this->options);

        $this->options = $options;
        
        return $this->options;
    }

    /**
     * @return array Searched value from Data Table filter with bind parameter
     */
    public function getSearchedValue() {

        return !empty($this->getPostData()->path('search.value')) ? ['searched_column' => "%" . $this->getPostData()->path('search.value') . "%"] : false;
    }

    /**
     * Search for order column name from data table AJAX post
     * 
     * @return string Order column name or FALSE if not found
     */
    public function getOrderColumn() {

        $dataColumns = $this->getColumns();

        $columnId = $this->getPostData()->path('order.0.column', false);

        if ($columnId === false) {
            return false;
        }

        $columnAlias = $dataColumns[$columnId]['name'];

        return $columnAlias;
    }

    /**
     * @return string Order direction
     */
    public function getOrderBy() {

        return $this->getPostData()->path('order.0.dir') ?
                strtoupper($this->getPostData()->path('order.0.dir')) :
                $this->getOrderDirection();
    }

    /**
     * @return array Columns needed to render the Data table
     */
    public function getColumns() {

        return $this->columns;
    }

    /**
     * Get the columns participate in db Query selection
     * 
     * @return array
     */
    public function getQueryColumns() {

        if (!empty($this->queryColumns)) {
            return $this->queryColumns;
        }

        $queryColumns = [];

        foreach ($this->getColumns() as $value) {

            if (empty($value['name'])) {
                continue;
            }
            $queryColumns[$value['data']] = $value['name'];
        }

        $this->queryColumns = $queryColumns;

        return $this->queryColumns;
    }

    /**
     * Get the conditions for Query search
     * 
     * @return string (column1 LIKE :searched_column: OR column2 LIKE :searched_column:)
     */
    public function getSearchQueryColumns() {

        if (!empty($this->searchQueryColumns)) {
            return $this->searchQueryColumns;
        }

        $string = [];

        foreach ($this->getColumns() as $value) {

            if (empty($value['searchable']) || empty($value['name'])) {
                continue;
            }

            $string[] = $value['name'] . ' LIKE :searched_column:';
        }

        $this->searchQueryColumns = implode(' OR ', $string);

        return $this->searchQueryColumns;
    }

    /**
     * Get data base column alias from array with query columns
     * 
     * @param string $column Column name
     */
    public function getQueryColumn($column = false) {

        $columns = $this->getQueryColumns();

        return !empty($column) && array_key_exists($column, $columns) ?
                $columns[$column] :
                false;
    }

    /**
     * Get DataTable options as JSON object. Load JS assets
     * Apply filter to remove the column name (it fix if mod_security is ON and the column name is COUNT()) 
     * 
     * @return string Data table options and Columns definitions to render the DataTable script and HTML
     */
    public function loadDataTableData() {

        $columns = $this->getColumns();

        if (empty($columns)) {
            return false;
        }

        $dtOptions = $this->getOptions();

        //search for default order column name and direction
        foreach ($this->getColumns() as $key => $value) {
            if (empty($value['defaultOrder']) || empty($value['orderDirection'])) {
                continue;
            }
            $dtOptions['order'] = [$key, $value['orderDirection']];
        }

        array_walk($columns, function(&$values) {
            if (isset($values['name'])) {
                unset($values['name']);
            }
        });

        $dtOptions['columns'] = $columns;
               
        return json_encode($dtOptions, JSON_NUMERIC_CHECK);
    }

}
