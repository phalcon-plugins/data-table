# data-table

**Phalcon DataTable plugin**

Allows simple interaction with DataTable using Phalcon Model or Phalcon Query builder.
The logic regarding to Query builder and search criteria is NOT closed into package.


*  Build your logic into backend - define the columns, columns mapping, search filters, limits , ordering, pagination
*  Run your query
*  Adds parameters to view
*  Load DataTable assets


** Example using Phalcon Query builder **


